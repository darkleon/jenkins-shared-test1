
def mycall(String buildStatus = 'STARTED') {
    List params = []
    List props = []

    com.cwctravel.hudson.plugins.extended_choice_parameter.ExtendedChoiceParameterDefinition test5 = new com.cwctravel.hudson.plugins.extended_choice_parameter.ExtendedChoiceParameterDefinition(
            "Action",
            "PT_SINGLE_SELECT",
            "Reinstall installer, Update components", //null,
            null,//project name
            null, //env.WORKSPACE+"/dependencies/local_instance_list.txt", //null,
            null,
            null, //env.WORKSPACE+"/dependencies/getBranchList.groovy",
            null,// bindings
            null,
            null, // propertykey
            null, //default value
            null,
            null,
            null,
            null, //default bindings
            null,
            null,
            null, //descriptionPropertyValue
            null,
            null,
            null,
            null,
            null,
            null,
            null,// javascript file
            null, // javascript
            false, // save json param to file
            false, // quote
            5, // visible item count
            "Installer action",
            ","

    )
    params << test5
    props << parameters(params)

    properties(props)

    println("${buildStatus}")
}